
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const exphbs = require('express-handlebars');
const session = require('express-session');
const validator = require('express-validator');
const passport = require('passport');
const flash = require('connect-flash');
const axios = require('axios');
const { parseCookies, setCookie } = require('nookies');
const bodyParser = require('body-parser');

const app = express();


// Settings
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    defaultLayout: 'index',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars')
}))
app.set('view engine', '.hbs');

// Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
function authenticationToken(req, res, next) {
    const jwt = parseCookies({ req }).jwt
    if (!jwt) {
        return res.redirect('/home')
    }
    next()
}
//endpoints
app.get('/home', async (req, res) => {
    const { data } = await axios.get('http://localhost:1337/api/to-dos');
    res.render('home/list', { tasks: data.data });
})
app.get('/signin', async (req, res) => {
    res.render('register/add')
})
app.post('/signin', async (req, res) => {
    const { identifier, password } = req.body;
    if (!identifier || !password) { res.status(409).json({ msg: 'all must be required' }) }
    const user = {
        identifier: identifier,
        password: password
    }
    const _auth = await axios.post("http://localhost:1337/api/auth/local",
        user,
        {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        }
    ).then(res => res.data).catch((err) => res.redirect('/signin'))
    setCookie({ res }, 'jwt', _auth.jwt, {
        maxAge: 60,
        path: '/',
    })
    res.redirect('/dashboard');
})
app.get('/dashboard', authenticationToken, async (req, res) => {
    res.render('admin/form')
})
app.post('/upload', authenticationToken, async (req, res) => {
    const { title } = req.body;
    if (!title) {
        return res.redirect('/dashboard')
    }
    const task = { data: { title } }
    const jwt = parseCookies({ req }).jwt
    console.log(">Datos", jwt, task);
    await axios.post('http://localhost:1337/api/to-dos', task,
        {
            headers: {
                Authorization: `Bearer ${jwt}`
            }
        }).then((res) => {
            console.log(res.data);
        }).catch((err) => {
            console.log(err);
        })
    res.redirect("/home");
})

// Public
app.use(express.static(path.join(__dirname, 'public')));

// Starting
app.listen(app.get('port'), () => {
    console.log('Server is in port', app.get('port'));
});